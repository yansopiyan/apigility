<?php
return [
    'Api\\V1\\Rest\\Roles\\Controller' => [
        'description' => 'roles',
    ],
    'Api\\V1\\Rest\\Employee\\Controller' => [
        'description' => 'data karyawan',
    ],
    'Api\\V1\\Rest\\Users\\Controller' => [
        'description' => 'data user',
    ],
    'Api\\V1\\Rest\\Country\\Controller' => [
        'description' => 'data negara',
    ],
    'Api\\V1\\Rest\\SupplierCategories\\Controller' => [
        'description' => 'kategori supplier',
    ],
    'Api\\V1\\Rest\\CustomBroker\\Controller' => [
        'description' => 'data custom broker',
    ],
    'Api\\V1\\Rest\\Groups\\Controller' => [
        'description' => 'data grouping user',
    ],
    'Api\\V1\\Rest\\Positions\\Controller' => [
        'description' => 'positions',
    ],
    'Api\\V1\\Rest\\GroupsRoles\\Controller' => [
        'description' => 'data role yang dimiliki group',
    ],
    'Api\\V1\\Rest\\Suppliers\\Controller' => [
        'description' => 'data master supplier',
    ],
    'Api\\V1\\Rest\\Products\\Controller' => [
        'description' => 'data master product',
    ],
    'Api\\V1\\Rest\\ProductCategories\\Controller' => [
        'description' => 'product category',
    ],
];
