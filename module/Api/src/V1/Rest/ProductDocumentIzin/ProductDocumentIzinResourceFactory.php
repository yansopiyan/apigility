<?php
namespace Api\V1\Rest\ProductDocumentIzin;

class ProductDocumentIzinResourceFactory
{
    public function __invoke($services)
    {
        return new ProductDocumentIzinResource();
    }
}
