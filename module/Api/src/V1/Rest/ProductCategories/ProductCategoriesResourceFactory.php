<?php
namespace Api\V1\Rest\ProductCategories;

class ProductCategoriesResourceFactory
{
    public function __invoke($services)
    {
        return new ProductCategoriesResource();
    }
}
