<?php
namespace Api\V1\Rest\Expeditions;

class ExpeditionsResourceFactory
{
    public function __invoke($services)
    {
        return new ExpeditionsResource();
    }
}
