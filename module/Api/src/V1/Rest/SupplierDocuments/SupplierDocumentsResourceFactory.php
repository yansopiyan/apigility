<?php
namespace Api\V1\Rest\SupplierDocuments;

class SupplierDocumentsResourceFactory
{
    public function __invoke($services)
    {
        return new SupplierDocumentsResource();
    }
}
