<?php
namespace Api\V1\Rest\Units;

class UnitsResourceFactory
{
    public function __invoke($services)
    {
        return new UnitsResource();
    }
}
