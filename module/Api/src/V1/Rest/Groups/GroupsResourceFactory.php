<?php
namespace Api\V1\Rest\Groups;

class GroupsResourceFactory
{
    public function __invoke($services)
    {
        return new GroupsResource();
    }
}
