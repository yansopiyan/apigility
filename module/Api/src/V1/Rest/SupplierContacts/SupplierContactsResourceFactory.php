<?php
namespace Api\V1\Rest\SupplierContacts;

class SupplierContactsResourceFactory
{
    public function __invoke($services)
    {
        return new SupplierContactsResource();
    }
}
