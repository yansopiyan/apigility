<?php
namespace Api\V1\Rest\Roles;

class RolesResourceFactory
{
    public function __invoke($services)
    {
        return new RolesResource();
    }
}
