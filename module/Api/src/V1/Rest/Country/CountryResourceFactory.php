<?php
namespace Api\V1\Rest\Country;

class CountryResourceFactory
{
    public function __invoke($services)
    {
        return new CountryResource();
    }
}
