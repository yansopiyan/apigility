<?php
namespace Api\V1\Rest\CustomerContracts;

class CustomerContractsResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerContractsResource();
    }
}
