<?php
namespace Api\V1\Rest\CompanyDivisions;

class CompanyDivisionsResourceFactory
{
    public function __invoke($services)
    {
        return new CompanyDivisionsResource();
    }
}
