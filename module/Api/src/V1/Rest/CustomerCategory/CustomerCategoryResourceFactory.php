<?php
namespace Api\V1\Rest\CustomerCategory;

class CustomerCategoryResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerCategoryResource();
    }
}
