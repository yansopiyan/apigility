<?php
namespace Api\V1\Rest\Employee;

class EmployeeResourceFactory
{
    public function __invoke($services)
    {
        return new EmployeeResource();
    }
}
