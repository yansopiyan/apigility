<?php
namespace Api\V1\Rest\PurchaseOrderSupplier;

class PurchaseOrderSupplierResourceFactory
{
    public function __invoke($services)
    {
        return new PurchaseOrderSupplierResource();
    }
}
