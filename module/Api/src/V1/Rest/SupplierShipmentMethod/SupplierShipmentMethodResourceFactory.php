<?php
namespace Api\V1\Rest\SupplierShipmentMethod;

class SupplierShipmentMethodResourceFactory
{
    public function __invoke($services)
    {
        return new SupplierShipmentMethodResource();
    }
}
