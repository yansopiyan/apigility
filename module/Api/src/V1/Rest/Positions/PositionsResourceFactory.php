<?php
namespace Api\V1\Rest\Positions;

class PositionsResourceFactory
{
    public function __invoke($services)
    {
        return new PositionsResource();
    }
}
