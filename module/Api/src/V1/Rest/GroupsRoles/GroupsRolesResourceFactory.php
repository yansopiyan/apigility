<?php
namespace Api\V1\Rest\GroupsRoles;

class GroupsRolesResourceFactory
{
    public function __invoke($services)
    {
        return new GroupsRolesResource();
    }
}
