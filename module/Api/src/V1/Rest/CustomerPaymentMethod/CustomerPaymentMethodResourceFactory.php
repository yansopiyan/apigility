<?php
namespace Api\V1\Rest\CustomerPaymentMethod;

class CustomerPaymentMethodResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerPaymentMethodResource();
    }
}
