<?php
namespace Api\V1\Rest\ExpeditionTypes;

class ExpeditionTypesResourceFactory
{
    public function __invoke($services)
    {
        return new ExpeditionTypesResource();
    }
}
