<?php
namespace Api\V1\Rest\EmployeeAreas;

class EmployeeAreasResourceFactory
{
    public function __invoke($services)
    {
        return new EmployeeAreasResource();
    }
}
