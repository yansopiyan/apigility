<?php
namespace Api\V1\Rest\Brands;

class BrandsResourceFactory
{
    public function __invoke($services)
    {
        return new BrandsResource();
    }
}
