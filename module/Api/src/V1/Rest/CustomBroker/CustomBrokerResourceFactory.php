<?php
namespace Api\V1\Rest\CustomBroker;

class CustomBrokerResourceFactory
{
    public function __invoke($services)
    {
        return new CustomBrokerResource();
    }
}
