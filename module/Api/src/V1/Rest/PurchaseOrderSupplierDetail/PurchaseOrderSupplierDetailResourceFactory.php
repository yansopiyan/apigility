<?php
namespace Api\V1\Rest\PurchaseOrderSupplierDetail;

class PurchaseOrderSupplierDetailResourceFactory
{
    public function __invoke($services)
    {
        return new PurchaseOrderSupplierDetailResource();
    }
}
