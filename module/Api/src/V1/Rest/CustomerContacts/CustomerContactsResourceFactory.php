<?php
namespace Api\V1\Rest\CustomerContacts;

class CustomerContactsResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerContactsResource();
    }
}
