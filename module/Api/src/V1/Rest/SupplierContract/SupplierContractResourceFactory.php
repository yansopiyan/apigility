<?php
namespace Api\V1\Rest\SupplierContract;

class SupplierContractResourceFactory
{
    public function __invoke($services)
    {
        return new SupplierContractResource();
    }
}
