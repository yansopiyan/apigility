<?php
namespace Api\V1\Rest\CustomerPaymentAvailable;

class CustomerPaymentAvailableResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerPaymentAvailableResource();
    }
}
