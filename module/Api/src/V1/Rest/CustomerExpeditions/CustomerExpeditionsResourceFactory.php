<?php
namespace Api\V1\Rest\CustomerExpeditions;

class CustomerExpeditionsResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerExpeditionsResource();
    }
}
