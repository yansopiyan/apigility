<?php
namespace Api\V1\Rest\CustomerAreas;

class CustomerAreasResourceFactory
{
    public function __invoke($services)
    {
        return new CustomerAreasResource();
    }
}
