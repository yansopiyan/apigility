<?php
namespace Api\V1\Rpc\NotificationWaktuTukarFaktur;

class NotificationWaktuTukarFakturControllerFactory
{
    public function __invoke($controllers)
    {
        return new NotificationWaktuTukarFakturController();
    }
}
