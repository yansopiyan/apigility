<?php
namespace Api\V1\Rpc\Users_changepassword;

class Users_changepasswordControllerFactory
{
    public function __invoke($controllers)
    {
        return new Users_changepasswordController();
    }
}
