<?php
namespace Api\V1\Rpc\Oauth;

class OauthControllerFactory
{
    public function __invoke($controllers)
    {
        return new OauthController();
    }
}
