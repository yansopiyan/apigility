<?php
namespace Api\V1\Rpc\NotificationSetting;

class NotificationSettingControllerFactory
{
    public function __invoke($controllers)
    {
        return new NotificationSettingController();
    }
}
