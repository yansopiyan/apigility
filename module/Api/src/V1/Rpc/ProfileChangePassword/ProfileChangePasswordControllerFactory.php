<?php
namespace Api\V1\Rpc\ProfileChangePassword;

class ProfileChangePasswordControllerFactory
{
    public function __invoke($controllers)
    {
        return new ProfileChangePasswordController();
    }
}
