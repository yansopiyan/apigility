<?php
namespace Api\V1\Rpc\SupplierCodeDocument;

class SupplierCodeDocumentControllerFactory
{
    public function __invoke($controllers)
    {
        return new SupplierCodeDocumentController();
    }
}
