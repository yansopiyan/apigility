<?php
namespace Api\V1\Rpc\ProductOrderCategories;

class ProductOrderCategoriesControllerFactory
{
    public function __invoke($controllers)
    {
        return new ProductOrderCategoriesController();
    }
}
