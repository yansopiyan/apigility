<?php
namespace Api\V1\Rpc\Notifications;

class NotificationsControllerFactory
{
    public function __invoke($controllers)
    {
        return new NotificationsController();
    }
}
