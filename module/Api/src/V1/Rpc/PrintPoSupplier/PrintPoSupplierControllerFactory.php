<?php
namespace Api\V1\Rpc\PrintPoSupplier;

class PrintPoSupplierControllerFactory
{
    public function __invoke($controllers)
    {
        return new PrintPoSupplierController();
    }
}
