<?php
namespace Api\V1\Rpc\UserRoles;

class UserRolesControllerFactory
{
    public function __invoke($controllers)
    {
        return new UserRolesController();
    }
}
