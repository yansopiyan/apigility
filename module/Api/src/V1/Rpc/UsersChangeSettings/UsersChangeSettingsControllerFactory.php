<?php
namespace Api\V1\Rpc\UsersChangeSettings;

class UsersChangeSettingsControllerFactory
{
    public function __invoke($controllers)
    {
        return new UsersChangeSettingsController();
    }
}
