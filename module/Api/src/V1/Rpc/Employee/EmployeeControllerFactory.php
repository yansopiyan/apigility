<?php
namespace Api\V1\Rpc\Employee;

class EmployeeControllerFactory
{
    public function __invoke($controllers)
    {
        return new EmployeeController();
    }
}
