<?php
namespace Api\V1\Rpc\SupplierCategories;

class SupplierCategoriesControllerFactory
{
    public function __invoke($controllers)
    {
        return new SupplierCategoriesController();
    }
}
