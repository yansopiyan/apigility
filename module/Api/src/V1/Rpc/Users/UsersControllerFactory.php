<?php
namespace Api\V1\Rpc\Users;

class UsersControllerFactory
{
    public function __invoke($controllers)
    {
        return new UsersController();
    }
}
