<?php
namespace Api\V1\Rpc\ProductMovingType;

class ProductMovingTypeControllerFactory
{
    public function __invoke($controllers)
    {
        return new ProductMovingTypeController();
    }
}
