<?php
namespace Api\V1\Rpc\NotificationWaktuAmbilBg;

class NotificationWaktuAmbilBgControllerFactory
{
    public function __invoke($controllers)
    {
        return new NotificationWaktuAmbilBgController();
    }
}
