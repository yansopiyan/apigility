<?php
namespace Api\V1\Rpc\ProductTypes;

class ProductTypesControllerFactory
{
    public function __invoke($controllers)
    {
        return new ProductTypesController();
    }
}
