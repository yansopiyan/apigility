<?php
namespace Api\V1\Rpc\SupplierPayment;

class SupplierPaymentControllerFactory
{
    public function __invoke($controllers)
    {
        return new SupplierPaymentController();
    }
}
