<?php
namespace Api\V1\Rpc\UsersChangePassword;

class UsersChangePasswordControllerFactory
{
    public function __invoke($controllers)
    {
        return new UsersChangePasswordController();
    }
}
