<?php
namespace Api\V1\Rpc\CustomerCategoryJenis;

class CustomerCategoryJenisControllerFactory
{
    public function __invoke($controllers)
    {
        return new CustomerCategoryJenisController();
    }
}
