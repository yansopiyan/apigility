<?php
namespace Api\V1\Rpc\Currencies;

class CurrenciesControllerFactory
{
    public function __invoke($controllers)
    {
        return new CurrenciesController();
    }
}
