<?php
namespace Api\V1\Rpc\Currency;

class CurrencyControllerFactory
{
    public function __invoke($controllers)
    {
        return new CurrencyController();
    }
}
