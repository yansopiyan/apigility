<?php
namespace Api\V1\Rpc\SupplierSubmission;

class SupplierSubmissionControllerFactory
{
    public function __invoke($controllers)
    {
        return new SupplierSubmissionController();
    }
}
