<?php
namespace Api\V1\Rpc\ProductCodeIzin;

class ProductCodeIzinControllerFactory
{
    public function __invoke($controllers)
    {
        return new ProductCodeIzinController();
    }
}
