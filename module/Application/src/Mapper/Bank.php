<?php

namespace Application\Mapper;

use Doctrine\ORM\EntityManager;

class Bank implements MapperInterface
{   
    protected $entityManager;
    protected $entityName;

    public function __construct(EntityManager $entityManager, $entityName)
    {
        $this->entityManager = $entityManager;
        $this->entityName = $entityName;
    }

    /**
     * findAll
     * @param array $orderBy
     * @return object|NULL
     */
    public function findAll($orderBy)
    {
        $er = $this->entityManager->getRepository($this->entityName);
        return $er->findBy(['deletedat' => NULL], $orderBy);
    }

    /**
     * findBy
     * @param array $criteria
     * @param array $orderBy
     * @return object|NULL
     */
    public function findBy($criteria, $orderBy)
    {
        $criteria['deletedat'] = NULL;
        $er = $this->entityManager->getRepository($this->entityName);
        return $er->findBy($criteria, $orderBy);
    }
    
    /**
     * Find
     * @param array $searchArray
     * @param array $queryParams
     */
    public function find($searchArray, $queryParams)
    {  
        
//         var_dump($searchArray); 
//         var_dump($queryParams);
//         exit();
        
        $sql = $this->entityManager->createQueryBuilder();
        
        if (isset($queryParams['count']) && $queryParams['count'] == true) {
            $sql->select('count(a) AS counting');
        } else {
            $sql->select('a');
        }
        
        $sql->from('\Application\Entity\Bank', 'a');
        
        if (isset($searchArray['active'])) {
            $sql->where($sql->expr()->eq('a.active', $searchArray['active']));
        }
        
        if (isset($queryParams['filter'])) {
            $filter = $sql->expr()->literal('%'.$queryParams['filter'][1].'%');
            $sql->andwhere($sql->expr()->like('a.'.$queryParams['filter'][0], $filter));
        }
        
        if (isset($queryParams['sort'])) {
            $orderField = array_keys($queryParams['sort'])[0];
            $sql->orderBy('a.'.$orderField, $queryParams['sort'][$orderField]);
        }
        
        if (isset($queryParams['first_result'])) {
            $sql->setFirstResult($queryParams['first_result']);
        }
        
        if (isset($queryParams['page_size'])) {
            $sql->setFirstResult($queryParams['first_result']);
            $sql->setMaxResults($queryParams['page_size']);
        }
        
        $sql->andwhere('a.deletedat is NULL');
        return $sql->getQuery()->getResult(\Doctrine\ORM\Query::HYDRATE_OBJECT);
    }
    
    /**
     * findOneBy
     * @param array $criteria
     * @return object|NULL
     */
    public function findOneBy($criteria)
    {
        $criteria['deletedat'] = NULL;
        $er = $this->entityManager->getRepository($this->entityName);
        return $er->findOneBy($criteria);
    }    

    /**
     * save
     * @param object $entity
     * @return boolean
     */
    public function save($entity)
    {
        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }
    
    /**
     * merge
     * @param object $entity
     * @return boolean
     */
    public function merge($entity)
    {
        try {
            $this->entityManager->merge($entity);
            $this->entityManager->flush();
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }  

}
