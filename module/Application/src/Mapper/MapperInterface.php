<?php

namespace Application\Mapper;

/**
 * This is an Interface for Mapper
 *
 * @since   0.1
 * @version $Revision$
 * @author  Dolly Aswin <dolly.aswin@gmail.com>
 *
 * @implements FactoryInterface
 */
interface MapperInterface
{
}
