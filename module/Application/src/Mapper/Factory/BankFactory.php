<?php

namespace Application\Mapper\Factory;

use Zend\ServiceManager\Factory\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Application\Mapper\Bank as BankMapper;

class BankFactory implements FactoryInterface
{
    public function __invoke(ServiceLocatorInterface $serviceLocator)
    {
        $mapper = new BankMapper(
            $serviceLocator->get('Application\ORM\EntityManager'),
            'Application\Entity\Bank'
        );

        return $mapper;
    }
}
