<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table(name="products", uniqueConstraints={@ORM\UniqueConstraint(name="uid_UNIQUE", columns={"uid"}), @ORM\UniqueConstraint(name="code_UNIQUE", columns={"product_id"})}, indexes={@ORM\Index(name="fk_produts_2_idx", columns={"category"}), @ORM\Index(name="fk_products_2_idx", columns={"supplier"}), @ORM\Index(name="fk_products_3_idx", columns={"unit"})})
 * @ORM\Entity
 */
class Products
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=64, nullable=false)
     */
    private $uid;

    /**
     * @var string
     *
     * @ORM\Column(name="product_id", type="string", length=20, nullable=false)
     */
    private $productId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=500, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=false)
     */
    private $active = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="barcode", type="string", length=250, nullable=true)
     */
    private $barcode;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discontinued", type="boolean", nullable=false)
     */
    private $discontinued = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity_per_unit", type="integer", nullable=true)
     */
    private $quantityPerUnit;

    /**
     * @var integer
     *
     * @ORM\Column(name="units_in_stock", type="integer", nullable=false)
     */
    private $unitsInStock = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="units_price", type="integer", nullable=true)
     */
    private $unitsPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=true)
     */
    private $weight;

    /**
     * @var string
     *
     * @ORM\Column(name="dimensions", type="string", length=200, nullable=true)
     */
    private $dimensions;

    /**
     * @var string
     *
     * @ORM\Column(name="material", type="string", length=200, nullable=true)
     */
    private $material;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Application\Entity\ProductCategories
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\ProductCategories")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var \Application\Entity\Supliers
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Supliers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="supplier", referencedColumnName="supplier_id")
     * })
     */
    private $supplier;

    /**
     * @var \Application\Entity\Units
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Units")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unit", referencedColumnName="id")
     * })
     */
    private $unit;


}

