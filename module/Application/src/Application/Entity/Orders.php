<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Orders
 *
 * @ORM\Table(name="orders", uniqueConstraints={@ORM\UniqueConstraint(name="uid_UNIQUE", columns={"uid"})}, indexes={@ORM\Index(name="fk_orders_1_idx", columns={"order_type"}), @ORM\Index(name="fk_orders_2_idx", columns={"customer"}), @ORM\Index(name="fk_orders_3_idx", columns={"employee"})})
 * @ORM\Entity
 */
class Orders
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=64, nullable=false)
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="order_date", type="date", nullable=false)
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="required_date", type="date", nullable=false)
     */
    private $requiredDate;

    /**
     * @var \Application\Entity\OrderType
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\OrderType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_type", referencedColumnName="id")
     * })
     */
    private $orderType;

    /**
     * @var \Application\Entity\Customers
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Customers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customer", referencedColumnName="customer_id")
     * })
     */
    private $customer;

    /**
     * @var \Application\Entity\Employees
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Employees")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="employee", referencedColumnName="id")
     * })
     */
    private $employee;


}

