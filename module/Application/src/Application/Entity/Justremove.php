<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Justremove
 *
 * @ORM\Table(name="justremove")
 * @ORM\Entity
 */
class Justremove
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=true)
     */
    private $name;


}

