<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GroupRole
 *
 * @ORM\Table(name="group_role", uniqueConstraints={@ORM\UniqueConstraint(name="uid_UNIQUE", columns={"uid"})}, indexes={@ORM\Index(name="fk_ group_role_1_idx", columns={"roles"}), @ORM\Index(name="fk_ group_role_2_idx", columns={"groups"})})
 * @ORM\Entity
 */
class GroupRole
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=64, nullable=false)
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Application\Entity\Roles
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Roles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="roles", referencedColumnName="id")
     * })
     */
    private $roles;

    /**
     * @var \Application\Entity\Groups
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Groups")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="groups", referencedColumnName="id")
     * })
     */
    private $groups;


}

