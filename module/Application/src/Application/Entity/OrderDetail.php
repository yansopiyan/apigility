<?php

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderDetail
 *
 * @ORM\Table(name="order_detail", uniqueConstraints={@ORM\UniqueConstraint(name="uid_UNIQUE", columns={"uid"})}, indexes={@ORM\Index(name="fk_order_detail_1_idx", columns={"order"}), @ORM\Index(name="fk_order_detail_2_idx", columns={"discount"})})
 * @ORM\Entity
 */
class OrderDetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=64, nullable=false)
     */
    private $uid;

    /**
     * @var integer
     *
     * @ORM\Column(name="product", type="integer", nullable=false)
     */
    private $product;

    /**
     * @var integer
     *
     * @ORM\Column(name="unit_price", type="integer", nullable=true)
     */
    private $unitPrice;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var \Application\Entity\Orders
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Orders")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order", referencedColumnName="id")
     * })
     */
    private $order;

    /**
     * @var \Application\Entity\Discounts
     *
     * @ORM\ManyToOne(targetEntity="Application\Entity\Discounts")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discount", referencedColumnName="id")
     * })
     */
    private $discount;


}

